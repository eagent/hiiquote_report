(defproject hiiquote-report "0.1.0-SNAPSHOT"
  :description "Creates a report using various hiiquote api."
  :url "http://example.com/FIXME"
  :license {:name "Restricted"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/tools.cli "0.4.1"]
                 [clj-http "3.9.1"]
                 [medley "1.0.0"]
                 [clj-time "0.14.4"]
                 [cheshire "5.8.1"]
                 [buddy "2.0.0"]]
  :main ^:skip-aot hiiquote-report.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
